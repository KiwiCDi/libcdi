int
find_trailing_space(char *buffer, unsigned int size)
{
    int i = size - 1;

    while (i > 0) {
        if (buffer[i] == ' ') {
            i--;
        } else {
            break;
        }
    }
    return i + 1;
}

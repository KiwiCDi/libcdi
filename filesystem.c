#include <errno.h>
#include <stdlib.h>

#include <cdio/cdio.h>

#include "common.h"
#include "directory.h"
#include "label.h"
#include "path_table.h"

#include "cdi.h"

struct cdi_file_descriptor {
    CdIo_t *cd;
    uint start_sector;
    uint8_t interleave[2];

    uint size;
    uint seek;
};

/* CD sector cache buffer */
char sector_cache_buffer[CDIO_CD_FRAMESIZE];

/* Stores what sector is cached on the buffer */
uint sector_cache = 0;

static struct cdi_path_table *
_find_directory_in_path_table(struct cdi_path_table *path_table, uint path_table_size,
        char *name, uint16_t parent_dir)
{
    uint i;

    for (i = 0; i < path_table_size; i++) {
        if (path_table[i].parent_directory == parent_dir
                && !strcmp(path_table[i].name, name)) {
            return &path_table[i];
        }
    }
    return NULL;
}

static struct cdi_file *
_find_file_in_directory(char *name, struct cdi_file *directory, uint directory_size)
{
    uint i;

    for (i = 0; i < directory_size; i++) {
        if (!strcmp(directory[i].record.filename, name)) {
            return &directory[i];
        }
    }
    return NULL;
}
struct cdi_filesystem
cdi_build_filesystem(CdIo_t *cd, int *exit_status)
{
    struct cdi_filesystem filesystem = {0};
    struct cdi_path_record *org_table = NULL; 
    struct cdi_path_table *table = NULL;
    uint path_table_size;

    char *buffer = NULL;
    int buffer_size;

    /* Read the Disc Label off the CD */
    if (!read_disc_label(cd, &filesystem.disc_label)) {
        if (exit_status) {
            *exit_status = CDIFS_DISC_LABEL_ERROR;
        }

        return filesystem;
    }

    /* Try to read and parse the Path Table */
    buffer_size = ROUND_BLOCK_SIZE(filesystem.disc_label.path_table_size, CDIO_CD_FRAMESIZE);
    buffer = malloc(buffer_size);

    if (!buffer || cdio_read_mode2_sectors(cd, buffer, filesystem.disc_label.path_table_addr, 0, buffer_size >> 11) != 0) {
        if (exit_status) {
            *exit_status = CDIFS_PATH_TABLE_READ_ERROR;
        }

        if (buffer) {
            free(buffer);
        }

        return filesystem;
    }

    org_table = parse_path_table(buffer, filesystem.disc_label.path_table_size, &path_table_size);

    if (!org_table) {
        if (exit_status) {
            *exit_status = CDIFS_PATH_TABLE_PARSE_ERROR;
        }

        free(buffer);
        return filesystem;
    }

    /* Parse all the directory lists, and build a path table off of them */
    table = malloc(path_table_size * sizeof(struct cdi_path_table));

    if (!table) {
        if (exit_status) {
            *exit_status = CDIFS_PATH_TABLE_ALLOC_ERROR;
        }

        free_path_table(org_table, path_table_size);
        free(org_table);
        free(buffer);
        return filesystem;
    }

    free(buffer);

    uint i;
    for (i = 0; i < path_table_size; i++) {
        table[i].name_size = org_table[i].name_size;
        table[i].name = strndup(org_table[i].name, org_table[i].name_size + 1);
        table[i].parent_directory = org_table[i].parent_dir;
        table[i].contents = parse_directory(cd, org_table[i].dir_address, &table[i].contents_size);
        table[i].xa_record_size = org_table[i].xa_record_size;
    }

    filesystem.cd = cd;
    filesystem.path_table = table;
    filesystem.path_table_size = path_table_size;
    filesystem.root_directory = table[0].contents;
    filesystem.root_directory_size = table[0].contents_size;

    if (exit_status) {
        *exit_status = 1;
    }

    free_path_table(org_table, path_table_size);
    free(org_table);
    return filesystem;
};

void
cdi_free_filesystem(struct cdi_filesystem filesystem)
{
    uint i;
    uint j;
    for (i = 0; i < filesystem.path_table_size; i++) {
        if (filesystem.path_table[i].name) {
            free(filesystem.path_table[i].name);
        }

        for (j = 0; j < filesystem.path_table[i].contents_size; j++) {
            free(filesystem.path_table[i].contents[j].record.filename);
        }
        free(filesystem.path_table[i].contents);
    }
    free(filesystem.path_table);
}

struct cdi_file_descriptor *
cdi_open(struct cdi_filesystem filesystem, char *path)
{
    char filename_buffer[128] = {0};
    uint buffer_size = 0;

    /* Root directory */
    struct cdi_path_table *cwd = filesystem.path_table;

    struct cdi_file *file = NULL;
    struct cdi_file_descriptor *fd = calloc(1, sizeof(struct cdi_file_descriptor));

    if (!fd) {
        return NULL;
    }

    while (*path) {
        if (*path == '/') {
            /* If the filename buffer is empty, then do nothing */
            if (buffer_size == 0) {
                path++;
                continue;
            }

            /* Null termination */
            filename_buffer[buffer_size++] = '\0';

            /* If the filename is '.', then do nothing */
            if (!strcmp(filename_buffer, ".")) {
                buffer_size = 0;
                continue;
            }

            /* Parent directory dots */
            if (!strcmp(filename_buffer, "..")) {
                cwd = &filesystem.path_table[cwd->parent_directory - 1];
                buffer_size = 0;
            }

            cwd = _find_directory_in_path_table(filesystem.path_table, filesystem.path_table_size,
                    filename_buffer, cwd->parent_directory);

            /* Directory not found */
            if (!cwd) {
                return NULL;
            }
            buffer_size = 0;
        } else {
            filename_buffer[buffer_size++] = *path;
        }
        path++;
    }

    if (buffer_size > 0) {
        filename_buffer[buffer_size++] = '\0';
        /* Search for the filename in the current working directory */
        file = _find_file_in_directory(filename_buffer, cwd->contents, cwd->contents_size);

        if (!file) {
            return NULL;
        }

        fd->cd = filesystem.cd;
        fd->start_sector = file->record.lbn_start;
        memcpy(fd->interleave, file->record.interleave, 2);
        fd->size = file->record.size;

        return fd;
    } else {
        /* Directory-only path, Not valid */
        return NULL;
    }
    return NULL;
}

int
cdi_read(struct cdi_file_descriptor *fd, char *buffer, uint size)
{
    /* TODO: Support interleaved files and CD-DA files */
    uint buffer_index = 0;

    if (!fd) {
        errno = EBADF;
        return -1;
    }

    /* End-of-file */
    if (fd->seek > fd->size) {
        return 0;
    }

    uint sector_to_read = fd->start_sector + (ROUND_BLOCK_SIZE(fd->seek, CDIO_CD_FRAMESIZE) >> 11);

    if (fd->seek > 0) {
        sector_to_read--;
    }

    uint sector_check = fd->seek;

    /* If sector is not cached yet, then read it from the CD and cache it into the buffer */
    if (sector_cache != sector_to_read) {
        if (cdio_read_mode2_sector(fd->cd, sector_cache_buffer, sector_to_read, 0) != 0) {
            errno = EIO;
            return -1;
        }
        sector_cache = sector_to_read;
    }

    if (size > (fd->size - fd->seek)) {
        size = fd->size - fd->seek;
    }

    uint index;
    uint read_size = size;
    while (size > 0) {
        index = fd->seek % CDIO_CD_FRAMESIZE;
        if (size > 2048) {
            read_size = 2048;
        } else {
            read_size = size;
        }

        memcpy(&buffer[buffer_index], &sector_cache_buffer[index], read_size);
        buffer_index += read_size;
        fd->seek += read_size;
        size -= read_size;

        if ((fd->seek - sector_check) >= 2048) {
            if (cdio_read_mode2_sector(fd->cd, sector_cache_buffer, ++sector_to_read, 0) != 0) {
                errno = EIO;
                return -1;
            }
            sector_cache = sector_to_read;
            sector_check = fd->seek;
        }
    }

    return buffer_index;
}

int
cdi_seek(struct cdi_file_descriptor *fd, uint seek)
{
    fd->seek = seek;
    return 0;
}

uint
cdi_size(struct cdi_file_descriptor *fd)
{
    return fd->size;
}

void
cdi_close(struct cdi_file_descriptor *fd)
{
    free(fd);
}


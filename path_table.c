#include <stdlib.h>

#include <cdio/cdio.h>

#include "common.h"
#include "path_table.h"

struct cdi_path_record *
parse_path_table(char *buffer, uint size, uint *res_size)
{
    uint32_t index = 0;
    uint32_t table_size = 0;
    struct cdi_path_record *record_list = malloc(sizeof(struct cdi_path_record));

    /* In case if realloc fails */
    struct cdi_path_record *backup_pointer = NULL;

    if (!record_list) {
        return NULL;
    }

    backup_pointer = record_list;

    char *name = NULL;

    while (index < size) {
        if (table_size != 0) {
            record_list = realloc(record_list, (table_size + 1) * sizeof(struct cdi_path_record));

            if (!record_list) {
                free(backup_pointer);
                return NULL;
            }
            
            backup_pointer = record_list;
        }

        record_list[table_size].name_size = buffer[index++];
        record_list[table_size].xa_record_size = buffer[index++];
        SET_BE_32(record_list[table_size].dir_address, buffer[index]);
        index += 4;
        SET_BE_16(record_list[table_size].parent_dir, buffer[index]);
        index += 2;

        name = calloc(1, record_list[table_size].name_size + 1);
        memcpy(name, &buffer[index], record_list[table_size].name_size);
        index += record_list[table_size].name_size;
        record_list[table_size].name = name;

        /* If the filename size is odd, then a null padding byte is inserted
         * to make it even, In case if this happens, skip a byte if the
         * filename size is odd */
        if (record_list[table_size].name_size % 2 == 1) {
            index++;
        }
        table_size++; 
    }

    if (res_size) {
        *res_size = table_size;
    }

    return record_list;
}

void
free_path_table(struct cdi_path_record *list, uint size)
{
    uint i;
    for (i = 0; i < size; i++) {
        if (list[i].name) {
            free(list[i].name);
        }
    }
}

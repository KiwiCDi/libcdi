#include <stdlib.h>

#include <cdio/cdio.h>

#include "common.h"
#include "directory.h"
#include "cdi.h"

struct cdi_file *
parse_directory(CdIo_t *cd, uint sector, uint *res_size)
{
    /* Sector buffer */
    char buffer[CDIO_CD_FRAMESIZE];

    if (cdio_read_mode2_sector(cd, buffer, sector, false) != 0) {
        return NULL;
    }

    /* Enough for the parser to get the file size of the directory
     * which helps us get the rest of the files */
    int size = 18;
    int index = 0;
    int table_size = 0;

    uint sector_index = 1;
    uint sector_left = 0;

    /* Filename buffer */
    char *name = NULL;

    struct cdi_file *file_list = malloc(sizeof(struct cdi_file));

    /* In case realloc fails */
    struct cdi_file *file_list_bak = NULL;

    if (!file_list) {
        return NULL;
    }

    file_list_bak = file_list;

    while ((index + ((sector_index - 1) * CDIO_CD_FRAMESIZE)) < size) {
        if ((buffer[index] == 0 || index == CDIO_CD_FRAMESIZE) && sector_index < sector_left) {
            /* Treat as if the rest of the data is going to be NULL */
            if (cdio_read_mode2_sector(cd, buffer, sector + (sector_index++), false) != 0) {
                free(file_list);
                return NULL;
            }

            index = 0;
        }

        file_list[table_size].record.record_size = buffer[index++];
        file_list[table_size].record.xa_record_size = buffer[index++];
        SET_BE_32(file_list[table_size].record.r, buffer[index]);
        index += 4;
        SET_BE_32(file_list[table_size].record.lbn_start, buffer[index]);
        index += 4;
        SET_BE_32(file_list[table_size].record.r2, buffer[index]);
        index += 4;
        SET_BE_32(file_list[table_size].record.size, buffer[index]);
        index += 4;

        /* If we are currently parsing the main directory, set the size to the
         * file size we received, and read more from the CD, if we should */
        if (table_size == 0) {
            size = file_list[table_size].record.size;
            sector_left = ROUND_BLOCK_SIZE(size, CDIO_CD_FRAMESIZE) / CDIO_CD_FRAMESIZE;
        }

        SET_STRING(file_list[table_size].record.creation_date, buffer[index], 6);
        index += 6;
        file_list[table_size].record.r3 = buffer[index++];
        file_list[table_size].record.flags = buffer[index++];
        SET_STRING(file_list[table_size].record.interleave, buffer[index], 2);
        index += 2;
        SET_BE_16(file_list[table_size].record.r4, buffer[index]);
        index += 2;
        SET_BE_16(file_list[table_size].record.set_seq, buffer[index]);
        index += 2;
        file_list[table_size].record.filename_size = buffer[index++];

        name = calloc(1, file_list[table_size].record.filename_size + 1);
        memcpy(name, &buffer[index], file_list[table_size].record.filename_size);
        index += file_list[table_size].record.filename_size;
        file_list[table_size].record.filename = name;

        /* If the filename size is even, then a null padding byte is inserted
         * to make it odd, In case if this happens, skip a byte if the
         * filename size is even */
        if (file_list[table_size].record.filename_size % 2 == 0) {
            index++;
        }

        SET_BE_32(file_list[table_size].record.owner_id, buffer[index]);
        index += 4;
        SET_BE_16(file_list[table_size].record.attributes, buffer[index]);
        index += 2;
        SET_BE_16(file_list[table_size].record.r5, buffer[index]);
        index += 2;
        file_list[table_size].record.filenum = buffer[index++];
        file_list[table_size].record.r6 = buffer[index++];
        
        table_size++;

        file_list = realloc(file_list, (table_size + 1) * sizeof(struct cdi_file));
        if (!file_list) {
            free(file_list_bak);
            return NULL;
        }

        file_list_bak = file_list;
    }

    if (res_size) {
        *res_size = table_size;
    }

    return file_list;
}

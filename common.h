#ifndef COMMON_H
#define COMMON_H

#include <byteswap.h>
#include <string.h>

#include "config.h"

#define uint unsigned int

#ifndef _BIGENDIAN
#define SET_BE_16(var, value) var = bswap_16(*((uint16_t*)&value))
#define SET_BE_32(var, value) var = bswap_32(*((uint32_t*)&value))
#else
#define SET_BE_16(var, value) var = *((uint16_t*)&value
#define SET_BE_32(var, value) var = *((uint32_t*)&value
#endif

#define SET_STRING(var, value, length) memcpy(&var, &value, length)
#define SET_STRING_NOSPACE(var, value, length) memcpy(&var, &value, find_trailing_space(&value, length))

#define ROUND_BLOCK_SIZE(n, s) ((n + s - 1) & ~(s - 1))

int find_trailing_space(char *buffer, unsigned int length);
#endif

#ifndef DIRECTORY_H
#define DIRECTORY_H

#define CDI_ATTRIBUTE_OWNER_READ    1
#define CDI_ATTRIBUTE_OWNER_EXECUTE 4
#define CDI_ATTRIBUTE_GROUP_READ    16
#define CDI_ATTRIBUTE_GROUP_EXECUTE 64
#define CDI_ATTRIBUTE_WORLD_READ    256
#define CDI_ATTRIBUTE_WORLD_EXECUTE 1024
#define CDI_ATTRIBUTE_CDDA          16384
#define CDI_ATTRIBUTE_DIR           32768

struct cdi_dir_record {
    uint8_t  record_size;
    uint8_t  xa_record_size;
    uint32_t r;
    uint32_t lbn_start;
    uint32_t r2;
    uint32_t size;
    uint8_t  creation_date[6];
    uint8_t  r3;
    uint8_t  flags;
    uint8_t  interleave[2];
    uint16_t r4;
    uint16_t set_seq;
    uint8_t  filename_size;
    char    *filename;
    uint32_t owner_id;
    uint16_t attributes;
    uint16_t r5;
    uint8_t  filenum;
    uint8_t  r6;
};

struct cdi_file *
parse_directory(CdIo_t *cd, uint sector, uint *res_size);

#endif

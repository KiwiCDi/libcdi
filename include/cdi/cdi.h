#ifndef CDI_H
#define CDI_H

#include <cdio/cdio.h>

#include "directory.h"
#include "label.h"

#define CDIFS_DISC_LABEL_ERROR       -1
#define CDIFS_PATH_TABLE_READ_ERROR  -2
#define CDIFS_PATH_TABLE_PARSE_ERROR -3
#define CDIFS_PATH_TABLE_ALLOC_ERROR -4

struct cdi_file_descriptor;

struct cdi_file {
    struct cdi_dir_record record;

    /* Directory only */
    uint16_t path_table_index;
};

struct cdi_path_table {
    uint8_t          name_size;
    char            *name;
    uint16_t         parent_directory;
    struct cdi_file *contents;
    uint32_t         contents_size;
    uint32_t         xa_record_size;
};

struct cdi_filesystem {
    CdIo_t                *cd;
    struct cdi_disc_label  disc_label; 
    struct cdi_path_table *path_table;
    uint32_t               path_table_size;
    struct cdi_file       *root_directory;
    uint32_t               root_directory_size;
};

struct cdi_filesystem cdi_build_filesystem(CdIo_t *cd, int *exit_status);
void cdi_free_filesystem(struct cdi_filesystem filesystem);
struct cdi_file_descriptor *cdi_open(struct cdi_filesystem filesystem, char *path);
int cdi_read(struct cdi_file_descriptor *fd, char *buffer, uint size);
uint cdi_size(struct cdi_file_descriptor *fd);
int cdi_seek(struct cdi_file_descriptor *fd, uint seek);
void cdi_close(struct cdi_file_descriptor *fd);
#endif

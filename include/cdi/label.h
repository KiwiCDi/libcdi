#ifndef LABEL_H
#define LABEL_H

#include <stdint.h>

struct cdi_record_header {
    uint8_t  record_type;
    char     id[5];
    uint8_t  version;
};

struct cdi_disc_label {
    struct cdi_record_header header;

    uint8_t  flag;
    char     system_id[32];
    char     volume_id[32];
    uint8_t  r[12];
    uint32_t size;
    uint8_t  charset_id[32];
    uint16_t r2;
    uint16_t num_vols;
    uint16_t r3;
    uint16_t set_seq;
    uint16_t r4;
    uint16_t lb_size;
    uint32_t r5;
    uint32_t path_table_size;
    uint8_t  r6[8];
    uint32_t path_table_addr;
    uint8_t  r7[38];
    char     album[128];
    char     publisher[128];
    char     dpreparer[128];
    char     app_id[128];
    char     copyright[32];
    uint8_t  r8[5];
    char     abstract[32];
    uint8_t  r9[5];
    char     biblio[32];
    uint8_t  r10[5];
    char     creation_date[16];
    uint8_t  r11;
    char     modification_date[16];
    uint8_t  r12;
    char     expiration_date[16];
    uint8_t  r13;
    char     effective_date[16];
    uint8_t  r14;
    uint8_t  file_version;
    uint8_t  reserved;
    uint8_t  app_use[512];
    uint8_t  r15[653];
};

struct cdi_term_record {
    struct cdi_record_header header;

    uint8_t                  reserved[2041]; /* must be all zero */
};

int read_disc_label(CdIo_t *cd, struct cdi_disc_label *label);
#endif

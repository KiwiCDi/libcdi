#include <string.h>

#include <cdio/cdio.h>

#include "common.h"
#include "label.h"

struct cdi_disc_label
parse_disc_label(char *buffer)
{
    struct cdi_disc_label label = {0};

    label.header.record_type = buffer[0];
    SET_STRING(label.header.id, buffer[1], 5);
    label.header.version = buffer[6];
    label.flag = buffer[7];
    SET_STRING_NOSPACE(label.system_id, buffer[8], 32);
    SET_STRING_NOSPACE(label.volume_id, buffer[40], 32);
    SET_STRING(label.r, buffer[72], 12);
    SET_BE_16(label.size, buffer[84]);
    SET_STRING(label.charset_id, buffer[88], 32);
    SET_BE_16(label.r2, buffer[120]);
    SET_BE_16(label.num_vols, buffer[122]);
    SET_BE_16(label.r3, buffer[124]);
    SET_BE_16(label.set_seq, buffer[126]);
    SET_BE_16(label.r4, buffer[128]);
    SET_BE_16(label.lb_size, buffer[130]);
    SET_BE_32(label.r5, buffer[132]);
    SET_BE_32(label.path_table_size, buffer[136]);
    SET_STRING(label.r6, buffer[140], 8);
    SET_BE_32(label.path_table_addr, buffer[148]);
    SET_STRING(label.r7, buffer[152], 38);
    SET_STRING_NOSPACE(label.album, buffer[190], 128);
    SET_STRING_NOSPACE(label.publisher, buffer[318], 128);
    SET_STRING_NOSPACE(label.dpreparer, buffer[446], 128);
    SET_STRING_NOSPACE(label.app_id, buffer[574], 128);
    SET_STRING_NOSPACE(label.copyright, buffer[702], 32);
    SET_STRING(label.r8, buffer[734], 5);
    SET_STRING_NOSPACE(label.abstract, buffer[739], 32);
    SET_STRING(label.r9, buffer[771], 5);
    SET_STRING_NOSPACE(label.biblio, buffer[776], 32);
    SET_STRING(label.r10, buffer[808], 5);
    SET_STRING(label.creation_date, buffer[813], 16);
    label.r11 = buffer[829];
    SET_STRING(label.modification_date, buffer[830], 16);
    label.r12 = buffer[846];
    SET_STRING(label.expiration_date, buffer[847], 16);
    label.r13 = buffer[863];
    SET_STRING(label.effective_date, buffer[864], 16);
    label.r14 = buffer[880];
    label.file_version = buffer[881];
    label.reserved = buffer[882];
    SET_STRING(label.app_use, buffer[883], 512);
    SET_STRING(label.r15, buffer[1395], 653);

    return label;
}

int
is_term_record(struct cdi_term_record record)
{
    int i;
    if (record.header.record_type != 255
            || strcmp(record.header.id, "CD-I ") == 0) {
        return 0;
    }

    for (i = 0; i < 2041; i++) {
        if (record.reserved[i] != 0) {
            return 0;
        }
    }
    return 1;
}

int
read_disc_label(CdIo_t *cd, struct cdi_disc_label *label)
{
    char buffer[CDIO_CD_FRAMESIZE];
    struct cdi_term_record term_record = {0};

    /* Read the disc label at sector 16 */
    if (cdio_read_mode2_sector(cd, &buffer, 16, false) != 0) {
        return 1;
    }
    *label = parse_disc_label(buffer);

    /* Read the terminator record at the next sector */
    if (cdio_read_mode2_sector(cd, &term_record, 17, false) != 0) {
        return 1;
    }

    return is_term_record(term_record);
}

#ifndef PATH_TABLE_H
#define PATH_TABLE_H

#include <stdint.h>

#include "common.h"

struct cdi_path_record {
    uint8_t  name_size;
    uint8_t  xa_record_size;
    uint32_t dir_address;
    uint16_t parent_dir;
    char    *name;
};

struct cdi_path_record *parse_path_table(char *buffer, uint size, uint *res_size);
void free_path_table(struct cdi_path_record *list, uint size);
#endif
